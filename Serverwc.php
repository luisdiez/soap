<?php

/**
 * Serverw class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class Serverwc extends SoapClient {

  private static $classmap = array(
                                   );

  public function Serverwc($wsdl = "http://localhost/~luisdiez/soap1/serviciow.wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param  
   * @return stringArray
   */
  public function getFamilias() {
    return $this->__soapCall('getFamilias', array(),       array(
            'uri' => 'http://localhost/~luisdiez/soap1/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param string $familia
   * @return stringArray
   */
  public function getProductosFamilia($familia) {
    return $this->__soapCall('getProductosFamilia', array($familia),       array(
            'uri' => 'http://localhost/~luisdiez/soap1/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param string $codigo
   * @param string $tienda
   * @return int
   */
  public function getStock($codigo, $tienda) {
    return $this->__soapCall('getStock', array($codigo, $tienda),       array(
            'uri' => 'http://localhost/~luisdiez/soap1/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param string $codigo
   * @return float
   */
  public function getPVP($codigo) {
    return $this->__soapCall('getPVP', array($codigo),       array(
            'uri' => 'http://localhost/~luisdiez/soap1/',
            'soapaction' => ''
           )
      );
  }

}

?>
