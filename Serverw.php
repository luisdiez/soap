<?php

require_once ('DB.php');
require_once ('Producto.php');

class Server{
	/**
	 * @return string[]
	 */
    public function getFamilias(){
		return DB::obtieneFamilias();
	}
	/**
	 * @param string $familia
	 * @return string[] 
	 */
	public function getProductosFamilia($familia){
		$productos = DB::obtieneProductosFamilia($familia);
		return $productos;
	}
	/**
	 * @param string $codigo
	 * @param string $tienda
	 * @return int 
	 */
	public function getStock($codigo, $tienda){
		$unidades = DB::obtieneStock($codigo, $tienda);
		return $unidades;
	}
	/**
	 * @param string $codigo
	 * @return float 
	 * 
	 */
	public function getPVP($codigo){
		$producto = DB::obtieneProducto($codigo); 
		$pvp = $producto->getPVP();
		return $pvp;
	}
}
?>