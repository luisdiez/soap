<?php
require_once ('DB.php');
require_once ('Producto.php');
class Server{
    public function getFamilias(){
		return DB::obtieneFamilias();
	}
	public function getProductosFamilia($familia){
		$productos = DB::obtieneProductosFamilia($familia);
		return $productos;
	}
	public function getStock($codigo, $tienda){
		$unidades = DB::obtieneStock($codigo, $tienda);
		return $unidades;
	}
	public function getPVP($codigo){
		$producto = DB::obtieneProducto($codigo); 
		$pvp = $producto->getPVP();
		return $pvp;
	}
}
?>